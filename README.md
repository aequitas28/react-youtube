# Simple React + React-Redux Youtube

This React project was created whilst learning React and uses a third party webservice which might cause issues.

This project is a mini clone of Youtube. Allowing user to search for some of their favourite videos on the Youtube platform.

### To get started:
1. Run command `npm install`
2. Run command `npm start`
    - This will start the dev server
3. Open browser and navigate to http://localhost:8080    

